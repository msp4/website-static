//# DocPad Configuration File
// http://docpad.org/docs/config

// Define the DocPad Configuration
docpadConfig = {
  collections: {
    shopper : function() {
      return this.getCollection("html").findAllLive({isItemMenu: true, layout: "shopper"},[{menuSort: 1}]);
    },
    client : function() {
      return this.getCollection("html").findAllLive({isItemMenu: true,layout: "client"},[{menuSort: 1}]);
    }
  },
  templateData: {
    site:{
      url: 'http://msp4.netlify.com'
    },
    shopperMenu: function() {
      return this.getCollection("shopper").toJSON();
    },
    clientMenu: function() {
      return this.getCollection("client").findAll({ subItem: {$exists:false}}).toJSON();
    },
    clientSubMenu: function() {
      return this.getCollection("client").toJSON();
    }
  },
  plugins: {
    handlebars: {
      helpers: {
        isActive: function(current,where){
          var item = this.id;
          if(!current || !where){
            return '';
          }
          else if (current.localeCompare(item) === 0){
            if (where.localeCompare("largeMenu")===0) return 'selected active';
            else if (where.localeCompare("largeMenuText")===0) return 'class=activePage';
            else if (where.localeCompare("subMenuText")===0) return 'active';
          }
          else if(where.localeCompare("largeMenu")===0) return 'sibling';
        },
        getBlock: function(item){
          return this.getBlock(item).toHTML();
        },
        getTitle: function(){
          if (this.navTitle) return this.navTitle;
          else return this.title;
        }
      },
      precompileOpts: {
        wrapper: "default"
      }
    },
    less: {
      compress: "true"
    },
    sitemap: {
      cachetime: 600000,
      changefreq: 'weekly',
      priority: 0.5,
      filePath: 'sitemap.xml'
    },
    multilang: {
      languages: ['en', 'ru'],
      defaultLanguage: 'ru',
      prettifyURL: false
    },
    livereload: {
      enabled: true
    },
    htmlmin:{
      removeComments: true,
      removeCommentsFromCDATA: false,
      removeCDATASectionsFromCDATA: false,
      collapseWhitespace: true,
      collapseBooleanAttributes: false,
      removeAttributeQuotes: false,
      removeRedundantAttributes: false,
      useShortDoctype: false,
      removeEmptyAttributes: false,
      removeOptionalTags: false,
      removeEmptyElements: false,
      environments: {
        development: {
          enabled: false
        }
      }
    }
  }
};

// Export the DocPad Configuration
module.exports = docpadConfig;