### Install

* Clone the repo
* Install node.js v0.12+ (version check: **node -v**)
* Install DocPad
* When upgrading: in addition to the above, run
* Run DocPad

```bash
git clone https://github.com/msp4/website-static.git && cd website-static
```

```bash
npm install -g npm; npm install -g docpad
docpad update
docpad run
```
