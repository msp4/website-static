var data = [
    {
        "country_code": "RU",
        "text": "Зарабатывай с нами от 300 руб в час",
        "img_src": "/media/design-a/img/leg_rubl_03_part.png",
        'btnClientsSubtext': "подарок от сервиса 1200 руб на ваш счет в системе"
    },
    {
        "country_code": "UA",
        "text": "Зарабатывай с нами от 50 гривен в час",
        "img_src": "/media/design-a/img/leg_hrivna_03_part.png",
        'btnClientsSubtext': "подарок от сервиса 200 грн на ваш счет в системе"
    },
    {
        "country_code": "BY",
        "text": "Зарабатывай с нами от 46 тысяч в час",
        "img_src": "/media/design-a/img/leg_euro_03_part.png",
        'btnClientsSubtext': "подарок от сервиса 184 тысячи на ваш счет в системе"
    },
    {
        "country_code": "ANOTHER",
        "text": "Зарабатывай с нами!",
        "img_src": "/media/design-a/img/leg_euro_03_part.png",
        'btnClientsSubtext': "полный контроль и без посредников"
    }
];

var urlData = [
    {
        "loggedInSite": "shopper",
        "redirectToUrl":"http://shopper.msp4.me",
        "linkText": "Мой кабинет"
    },
    {
        "loggedInSite": "client",
        "redirectToUrl":"http://client.msp4.me",
        "linkText": "Мой кабинет"
    }
];

var contactsData = ["UA", "RU", "USA"];

function updateContacts (contactsData, country_code){
    for (var i in contactsData) {
        if (country_code == contactsData[i]) {
            document.getElementById(contactsData[i]+"1").className = "active";
            document.getElementById(contactsData[i]).className = "tab-pane active";
            break;
        } else if (i == contactsData.length -1) {
            document.getElementById("UA"+"1").className = "active";
            document.getElementById("UA").className = "tab-pane active";
        }

    }
}

function updateData(data, country_code) {
    for (var i in data) {
        if (country_code == data[i].country_code) {
            document.getElementById('headImg').style.background = "url(\""+data[i].img_src+"\") no-repeat";
            if (location.pathname == "/") {
                var htb = document.getElementById("headText").getElementsByTagName("h1")[0].innerHTML = data[i].text;
            }
            break;
        } else if (i == (data.length -1)) {
            document.getElementById('headImg').style.background = "url(\""+data[i].img_src+"\") no-repeat";
        }
    }
}

function updateBtnClientSubtext (data, country_code) {
    for (var i in data) {
        if (country_code == data[i].country_code) {
            document.getElementById('clients-action-btn').innerHTML = data[i].btnClientsSubtext;
        }
    }
}

function updateLinks(urlData, loggedInSite) {
    for (var i in urlData) {
        if (urlData[i].loggedInSite == loggedInSite) {
            //link:
            document.getElementById("signInLink").innerHTML = urlData[i].linkText;
            document.getElementById("signInLink").setAttribute("data-target", "") ;
            document.getElementById("signInLink").setAttribute("data-toggle", "") ;
            document.getElementById("signInLink").href = urlData[i].redirectToUrl;

            //button:
            document.getElementById("signInButton").setAttribute("data-toggle", "") ;
            document.getElementById("signInButton").setAttribute("data-target", "") ;
            document.getElementById("signInButton").setAttribute("onclick", "") ;
            document.getElementById("signInButton").href = urlData[i].redirectToUrl;
        }
    }
}

//Function to get cookie:
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}

//When DOM is ready, run the functions:
$(document).ready(function () {
    //update text and image on the site header block depending on which country the visitor came from:
    if ('localStorage' in window && window.localStorage !== null) {
        if (!localStorage.getItem("country_code")) {
            $.get("http://ipinfo.io", function (response) {
                localStorage.setItem("country_code", response.country);
                updateData(data, response.country);
                updateBtnClientSubtext(data, response.country);
                if (location.pathname == "/nashi-kontakty/") {
                    updateContacts(contactsData, response.country);
                }
            }, "jsonp");
        }
        if (localStorage.getItem("country_code")) {
            updateData(data, localStorage.getItem("country_code"));
            updateBtnClientSubtext(data, localStorage.getItem("country_code"));
            if (location.pathname == "/nashi-kontakty/") {
                updateContacts(contactsData, localStorage.getItem("country_code"));
            }
        }
    } else {
        $.get("http://ipinfo.io", function (response) {
            updateData(data, response.country_code);
            updateBtnClientSubtext(data, response.country_code);
            if (location.pathname == "/nashi-kontakty/") {
                updateContacts(contactsData, response.country_code);
            }

        }, "jsonp");
    }

    //check user logged into the shopper dashboard or not(read cookie):
    if (getCookie("login")) {
        updateLinks(urlData, getCookie("login"));
    }

});
